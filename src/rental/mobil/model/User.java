/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.mobil.model;

import javax.swing.JOptionPane;
import rental.mobil.event.LoginListener;

/**
 *
 * @author Admiral
 */
public class User {
    String username,password,nama;
    int id,role;
//    boolean is_login;

    private LoginListener loginListener;
    
    public LoginListener getLoginListener() {
        return loginListener;
    }

    public void setLoginListener(LoginListener loginListener) {
        this.loginListener= loginListener;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

//    public boolean isIs_login() {
//        return is_login;
//    }
//
//    public void setIs_login(boolean is_login) {
//        this.is_login = is_login;
//    }
    
    private void fireOnChange() {
        if (loginListener != null) {
            loginListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setUsername("");
        setPassword("");
    }
    
    public void simpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        resetForm();
    }
    
    public void sudahAda(){
        JOptionPane.showMessageDialog(null, "Sudah aDa");
        resetForm();
    }
    
    
}
